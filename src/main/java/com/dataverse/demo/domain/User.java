package com.dataverse.demo.domain;

import javax.persistence.*;


    @Entity
    @Table(name = "USER", uniqueConstraints = {@UniqueConstraint(columnNames = {"firstname", "lastname", "email"})})
    public class User {

        private static final int MAX_LENGTH = 60;

        @Id
        @GeneratedValue
        @Column(name = "id", unique = true, nullable = false)
        private Long id;


        @Column(name = "firstname", length = MAX_LENGTH)
        private String firstName;


        @Column(name = "lastName", length = MAX_LENGTH)
        private String lastName;


        @Column(name = "email", length = MAX_LENGTH)
        private String email;


        @Column(name = "phoneNumber")
        private Integer phoneNumber;


        @Column(name = "company", length = MAX_LENGTH)
        private String company;


        @Column(name = "password", length = MAX_LENGTH)
        private String password;




        public User(Long id,
                           String firstName,
                           String lastName,
                           String email,
                           Integer phoneNumber,
                           String company,
                           String password
                           ) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.company = company;
            this.password = password;

        }

        public User() {

        }

        public int getMaxNameLength() {
            return MAX_LENGTH;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Integer getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(Integer phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public User get() {
            return null;
        }
    }

