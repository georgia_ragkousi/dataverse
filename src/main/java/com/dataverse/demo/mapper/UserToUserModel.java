package com.dataverse.demo.mapper;

import com.dataverse.demo.domain.User;
import com.dataverse.demo.model.UserModel;
import org.springframework.stereotype.Component;

@Component
public class UserToUserModel {
    public UserModel mapToUserModel(User user) {
        UserModel userModel = new UserModel();
        userModel.setFirstName(user.getFirstName());
        userModel.setLastName(user.getLastName());
        userModel.setEmail(user.getEmail());
        userModel.setCompany(user.getCompany());
        userModel.setPassword(user.getPassword());
        return userModel;
    }
}
