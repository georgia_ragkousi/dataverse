package com.dataverse.demo.controller;

import com.dataverse.demo.model.LoginResponse;
import com.dataverse.demo.model.UserModel;
import com.dataverse.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserProfileController {

    private static final String USER1="user1";

    @Autowired
    private UserService userService;

   @GetMapping({"userProfile"})
    public String user1(Model model) {

        List<UserModel> books = userService.findAll();
        model.addAttribute(USER1, books);

        return "/userProfile";
    }


}

