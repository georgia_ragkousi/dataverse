package com.dataverse.demo.controller;

import com.dataverse.demo.form.RegisterForm;
import com.dataverse.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class CreateUserController {

    private static final String REGISTER_FORM="registerForm";

    @Autowired
    private UserService userService;


    @GetMapping({"/register"})
    public String register (Model model) {
        model.addAttribute(REGISTER_FORM, new RegisterForm());
        return "/register";
    }

    @PostMapping({"/register"})
    public String register (@Valid @ModelAttribute(REGISTER_FORM)
                                    RegisterForm registerForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()){
          addPageAttribute(registerForm,bindingResult,redirectAttributes);
            return "redirect:/register";
        }
        userService.registerUser(registerForm);
        return "redirect:/home";
    }

   private void addPageAttribute(RegisterForm registerForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
    redirectAttributes.addFlashAttribute(REGISTER_FORM,registerForm);
    redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.registerForm", bindingResult);
    }






}
