package com.dataverse.demo.service;

import com.dataverse.demo.form.RegisterForm;
import com.dataverse.demo.model.UserModel;

import java.util.List;


public interface UserService {

    UserModel login();

void registerUser(RegisterForm registerForm);
  //  void registerUser(RegisterForm registerForm);

    UserModel findUserById(Long id);

    List<UserModel> findAll();
}
