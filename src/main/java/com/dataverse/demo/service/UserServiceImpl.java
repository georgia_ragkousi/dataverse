package com.dataverse.demo.service;

import com.dataverse.demo.SecurityConfig;
import com.dataverse.demo.domain.User;
import com.dataverse.demo.form.RegisterForm;
import com.dataverse.demo.mapper.UserToUserModel;
import com.dataverse.demo.model.UserModel;

import com.dataverse.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private UserToUserModel mapper;

    @Override
    public UserModel login() {
        return null;
    }

    @Override
    public void registerUser(RegisterForm registerForm) {

        User user=new User();

        user.setFirstName(registerForm.getFirstName());
        user.setLastName(registerForm.getLastName());
        user.setPassword(securityConfig.passwordEncoder().encode(registerForm.getPassword()));
        user.setCompany(registerForm.getCompany());
        user.setEmail(registerForm.getEmail());
        user.setPhoneNumber(Integer.parseInt(registerForm.getPhoneNumber()));

        userRepository.save(user);

    }


    //   @Override
  //  public void registerUser(RegisterForm registerForm) {
   //     userRepository.save(registerForm);
 //  }


    @Override
    public UserModel findUserById(Long id) {
        return null;
    }


     @Override
    public List<UserModel> findAll() {
        return userRepository
                .findAll()
                .stream()
                .map(user -> mapper.mapToUserModel(user))
                .collect(Collectors.toList());
    }




}
