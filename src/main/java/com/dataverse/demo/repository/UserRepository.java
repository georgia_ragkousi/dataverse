package com.dataverse.demo.repository;

import com.dataverse.demo.domain.User;
import com.dataverse.demo.form.RegisterForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface UserRepository extends JpaRepository<User, String> {
    void save(RegisterForm registerForm);

    User findByEmail(String username);

}
