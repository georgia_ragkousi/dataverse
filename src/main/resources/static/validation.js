jQuery(function ($){

    const registerForm = $('#registerForm');
    if (registerForm.validate) {
        registerForm.validate({
            rules: {

                firstName: {
                    required: true
                },

                lastName: {
                    required: true
                },

                email: {
                    required: true,
                    email: true
                },

                password:{
                    required: true,
                    minlength: 8,
                    passwordRegex:true
                },

                confirmation_password:{
                    required: true,
                    equalTo: '#password'
                },

                phoneNumber: {

                    digits: true,
                    minlength: 10,
                    maxlength: 10
                }
            },

            messages: {

                email: {
                    required: 'Please enter your email ',
                },
                firstName: {
                    required: 'Please enter your first name'
                },
                lastName: {
                    required: 'Please enter your last name'
                },
                password: {
                    required: 'Please enter your password'
                },

                confirmation_password:{
                required: 'Please confirm your password',
                equalTo: 'Please enter the same password as above'
                },

                phoneNumber: {
                    minlength: 'Phone number should be more than 10 digits',
                    maxlength: 'Phone number should be maxed out at 10 digits',
                    digits: 'Phone number should contain only digits'
                }
            }
        });
    }

});

jQuery.validator.addMethod("passwordRegex", function(value) {
return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]+$/.test(value)
              && /[a-z]/.test(value) || /[A-Z]/.test(value) // has a lowercase letter or capital
              && /\d/.test(value)//has a digit
              && /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value) // has a special character
             },"Password must consist at least one capital or lowercase letter,one number and one special character");
